Vue.component('MainComponent', require('@/components/MainComponent.vue').default);
Vue.component('Loading', require('@/components/Loading.vue').default);
Vue.component('StarshipList', require('@/components/StarshipList.vue').default);
Vue.component('PilotList', require('@/components/PilotList.vue').default);
Vue.component('PilotInfoDialog', require('@/components/PilotInfoDialog.vue').default);
Vue.component('HttpErrorAlerts', require('@/components/HttpErrorAlerts.vue').default);