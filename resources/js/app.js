window.Vue = require('vue').default;

// Custom Vuex (it's similar but not vuex) -> Observable pattern
import { state } from './store/state'
import { getters } from './store/getters'
import { actions } from './store/actions'
Vue.prototype.store = {
  state: state,
  actions: actions,
  getters: getters
};

// Vuetify
import '@mdi/font/css/materialdesignicons.css'
import Vuetify from 'vuetify';
Vue.use(Vuetify);
const vuetify = new Vuetify({
    theme: {
        dark: false,
        themes: {
            light: {
                primary: '#607d8b',
                secondary: '#ffc107',
                accent: '#3f51b5',
                error: '#f44336',
                warning: '#ff9800',
                info: '#00bcd4',
                success: '#4caf50',
                background: '#FAFAFA',
            },
            dark: {
                background: '#212121',
            }
        },
  },
});

require('./register-components');

const app = new Vue({
    el: '#app',
    vuetify: new Vuetify(),
});
