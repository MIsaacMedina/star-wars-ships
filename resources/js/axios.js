import axios from "axios";

let url = process.env.NODE_ENV === "production" ? "http://your_production_url.com" : "http://localhost:8000/api/";

const http = (_url = url) => {
  return axios.create({
    baseURL: _url,
    timeout: 200000,
    headers: {
      "Content-Type": "application/json;charset=UTF-8"
    }
  });
};

export default {
  url,
  http,
};
