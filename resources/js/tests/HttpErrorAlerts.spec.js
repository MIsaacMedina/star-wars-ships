import { mount, createLocalVue } from '@vue/test-utils'
import HttpErrorAlerts from '@/components/HttpErrorAlerts.vue'
import Vuetify from 'vuetify'

const localVue = createLocalVue();

describe('HttpErrorAlerts.vue', () => {
    const localVue = createLocalVue();
    let vuetify;
    let store;
    let wrapper;

    beforeEach(() => {
        vuetify = new Vuetify();
        store = {
            state: {
                errors: ['Error1','Error2','Error3'],
            }
        };
        const App = localVue.component('App', {
            components: { HttpErrorAlerts },
            template: `
                <v-app>
                    <HttpErrorAlerts
                        ref="httpErrorAlerts"
                    />
                </v-app>
            `,
        });
        wrapper = mount(App, { localVue, vuetify, mocks: { store } });
    });

    it('Renders component', async () => {
        await wrapper.vm.$nextTick();
        expect(wrapper.find('.v-alert').exists()).toBe(true);
    })

    it('Show multiple error alerts', async () => {
        await wrapper.vm.$nextTick();
        expect(wrapper.findAll('.v-alert')).toHaveLength(3);
    })
})