import axios from "../axios";
import { state } from "./state";
import { getters } from "./getters";

export const actions = {
    async GetPerson(personId) { 
        try {
            state.loading = true;
            state.dataError = [];
            const { data } = await axios.http().get(`people/${personId}`);
            state.loading = false;
            return data;
        } catch (e) {
            getters.$showError(e);
        }
    },
    async GetStarshipsDependencies() {
        try {
            state.loading = true;
            state.dataError = [];
            const { data } = await axios.http().get('starships/dependencies');
            state.loading = false;
            return data;
        } catch (e) {
            getters.$showError(e);
        }
     },
    async GetStarships() {
        try {
            state.loading = true;
            state.dataError = [];
            const { data } = await axios.http().get('starships');
            state.loading = false;
            return data;
        } catch (e) {
            getters.$showError(e);
        }
    },
    async RemovePilotToStarship(ship, pilot) {
        try {
            state.loading = true;
            state.dataError = [];
            const { data } = await axios.http().delete(`starships/${ship}/${pilot}`);
            state.loading = false;
            return data;
        } catch (e) {
            getters.$showError(e);
        }
    },
    async AddPilotToStarship(ship, pilot) {
        try {
            state.loading = true;
            state.dataError = [];
            const { data } = await axios.http().post(`starships/${ship}/${pilot}`);
            state.loading = false;
            return data;
        } catch (e) {
            getters.$showError(e);
        }
    },
}