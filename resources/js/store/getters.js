import { state } from "./state";

export const getters = {
    $showError: (e) => {
        if (process.env.NODE_ENV !== 'production') {
            console.error(e)
        }
        if (e.response && e.response.data) {
            state.errors.push(`${e.response.status} ${e.response.statusText}${e.response.data.message ? `: ${e.response.data.message}` : ''}`);
        }
        state.loading = false;
    },
}
