import Vue from 'vue';

export const state = Vue.observable({
    loading: false,
    errors: [],
    theme: null,
});