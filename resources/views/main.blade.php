<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <!-- Metas -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Star Wars Ships</title>

        <!-- Styles -->
        <link href="/css/app.css" rel="stylesheet">
        
        <!-- Scripts -->
        <script src="../js/app.js" defer></script>
    </head>
    <body>
        <div id="app">
            <main-component />
        </div>
    </body>
</html>
