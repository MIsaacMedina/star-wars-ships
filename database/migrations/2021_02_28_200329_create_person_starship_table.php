<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePersonStarshipTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('person_starship', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('person_id');
            $table->unsignedBigInteger('starship_id');
        });

        Schema::table('person_starship', function (Blueprint $table) {
            $table->foreign('person_id')->references('id')->on('people')->onDelete('cascade');
            $table->foreign('starship_id')->references('id')->on('starships')->onDelete('cascade');
            $table->unique(["person_id", "starship_id"], 'person_starship_unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('person_starship', function (Blueprint $table) {
            $table->dropForeign(['person_id']);
            $table->dropForeign(['starship_id']);
            $table->dropUnique('person_starship_unique');
        });
        Schema::dropIfExists('person_starship');
    }
}
