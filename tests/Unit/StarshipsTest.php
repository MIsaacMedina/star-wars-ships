<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;
use App\Models\Starship;

class StarshipsTest extends TestCase
{
    public function test_price_is_base15()
    {
        $starship = new Starship([
            'name' => 'My ship',
            'model' => 'My model',
            'manufacturer' => 'My manufacturer',
            'class' => 'My class',
            'price' => 100.00,
        ]);
        $this->assertEquals($starship->price, "6¶.0");
    }
}
