<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;
use App\Models\Person;
use App\Models\Starship;

class ApiTest extends TestCase
{
    use WithoutMiddleware;
    
    public function test_get_pilot()
    {
        $response = $this->get('/api/people/1');
        $response
            ->assertStatus(200)
            ->assertJson([
                'id' => 1,
            ]);
    }

    public function test_not_allowed_remove_ship() {
        $response = $this->delete('/api/starships/1')
            ->assertStatus(405);
    }

    public function test_not_allowed_remove_pilot() {
        $response = $this->delete('/api/people/1')
            ->assertStatus(405);
    }

    public function test_get_starships_dependencies() {
        $response = $this->get('/api/starships/dependencies')
            ->assertStatus(200)
            ->assertJson([
                'people' => [],
            ]);
    }
}
