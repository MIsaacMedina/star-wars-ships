<img src="https://1000marcas.net/wp-content/uploads/2019/12/Star-Wars-Logo.png" width="200" alt="Star Wars Logo" style="margin-left: calc((100% - 200px) / 2);">

## About

**Star Wars Ships** is a project in which we can see the ships of Start Wars and their pilots. We can also remove and add pilots to the ships.

## Technologies

This project uses **Laravel** framework as backend API, **VueJS** as frontend, **Vuetify** for easy styling and **SWAPI** data to populate the database tables.

-   [Laravel v8.29](https://laravel.com/docs/8.x).
-   [VueJS v2.6.12](https://es.vuejs.org/v2/guide/).
-   [Vuetify v2.4.5](https://vuetifyjs.com/en/introduction/roadmap/).
-   [SWAPI](https://swapi.dev/).

## How run

Create a batabase and user. Add this informacion in the .env file.

Create tables.

```bash
php artisan migrate
```

Populate tables with SWAPI information.

```bash
php artisan database:import_from_swapi
```

Serve artisan

```bash
php artisan serve
```

Install npm dependencies.

```bash
npm install
```

Compile the project.

```bash
npm run dev
```

If you want to do changes and you don't want recompile, run the watch instead of dev.

```bash
npm run watch
```

Run PHP tests.

```bash
php artisan test ./tests
```

Run VueJS tests.

```bash
npm test
```
