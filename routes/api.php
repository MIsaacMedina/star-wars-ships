<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\StarshipController;
use App\Http\Controllers\PersonController;

Route::get('starships', [StarshipController::class, 'index']);
Route::get('starships/dependencies', [StarshipController::class, 'dependencies']);
Route::get('starships/{shipId}', [StarshipController::class, 'show']);
Route::post('starships/{shipId}/{pilotId}', [StarshipController::class, 'starshipPilot']);
Route::delete('starships/{shipId}/{pilotId}', [StarshipController::class, 'destroyStarshipPilot']);
Route::resource('people', PersonController::class, [ 'only'=>['index','show'] ]);
