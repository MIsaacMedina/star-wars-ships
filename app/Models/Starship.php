<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Starship extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'model',
        'price'
    ];

    public function people()
    {
        return $this->belongsToMany(Person::class);
    }

    /**
     * Price Accessor: Custom base15 conversion.
     *
     * @return string
     */
    public function getPriceAttribute($value) 
    {
        $integer = base_convert(floor($value), 10, 15);
        $decimal = base_convert(substr($value,-2), 10, 15);
        $search = ['a', 'b', 'c', 'd', 'e'];
        $replace = ['¶', 'μ', '¢', 'Þ', 'ß'];
        $integer = str_replace($search, $replace, $integer);
        $decimal = str_replace($search, $replace, $decimal);
        return $integer.'.'.$decimal;
    }
}
