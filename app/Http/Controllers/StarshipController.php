<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Starship;
use App\Models\Person;

class StarshipController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Starship::with('people:id,name')->get();
    }

    /**
     * Obtain dependencies.
     *
     * @return \Illuminate\Http\Response
     */
    public function dependencies()
    {
        $people = Person::select('id','name')->get();
        return [ 'people' => $people ];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Starship::where('id', $id)->first();
    }

    /**
     * Add relation between pilot and starship.
     *
     * @param  int  $shipId
     * @param  int  $pilotId
     * @return \Illuminate\Http\Response
     */
    public function starshipPilot($shipId, $pilotId)
    {
        $ship = Starship::where('id', $shipId)->first();
        $ship->people()->attach($pilotId);

        return Starship::with('people')->get();
    }

    /**
     * Remove relation between pilot and starship.
     *
     * @param  int  $shipId
     * @param  int  $pilotId
     * @return \Illuminate\Http\Response
     */
    public function destroyStarshipPilot($shipId, $pilotId)
    {
        $db_ship = Starship::where('id',$shipId)->first();
        $db_ship->people()->detach($pilotId);
        return Starship::with('people')->get();
    }
}
