<?php

namespace App\Console\Commands;

use Illuminate\Support\facades\Http;
use Illuminate\Support\Facades\DB;
use Illuminate\Console\Command;
use App\Models\Person;
use App\Models\Starship;


class ImportFromSwapi extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'database:import_from_swapi';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import data from SWAPI to our database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        echo "Importing data from SWAPI...\n";
        DB::transaction(function()
        {
            if(!$this->get_people()) return;
            if(!$this->get_starships()) return;

            echo "\e[1;37;42mImport successful!\e[0m\n";
        });
        return 0;
    }

    private function get_people() {
        $url = "https://swapi.dev/api/people/";

        // Truncate table people
        DB::statement("SET foreign_key_checks=0");
        Person::truncate();
        DB::statement("SET foreign_key_checks=1");

        $data = $this->get_data_from_url($url);
        if(!$data) return false;
        $next = $data["next"];

        // Add new data
        while($data) {
            foreach ($data["results"] as $person) 
            {
                $newPerson = new Person;
                $newPerson->name = $person["name"];
                $newPerson->genre = $person["gender"];
                $newPerson->birth_year = $person["birth_year"];
                $url = $person["homeworld"];
                if ($url) {
                    $homeworld = $this->get_data_from_url($url);
                    $newPerson->homeworld = $homeworld["name"];
                }
                $newPerson->save();
            }

            $url = $data["next"];
            $data = $url ? $this->get_data_from_url($url) : null;
        }

        return true;
    }

    private function get_starships() {
        $url = "https://swapi.dev/api/starships/";

        // Truncate table starships and person_starship (intermediate many to many)
        DB::statement("SET foreign_key_checks=0");
        Starship::truncate();
        DB::table('person_starship')->truncate();
        DB::statement("SET foreign_key_checks=1");

        $data = $this->get_data_from_url($url);
        if(!$data) return false;
        $next = $data["next"];

        // Add new data
        while($data) {
            foreach ($data["results"] as $starship) 
            {
                $newStarship = new Starship;
                $newStarship->name = $starship["name"];
                $newStarship->model = $starship["model"];
                $newStarship->manufacturer = $starship["manufacturer"];
                $newStarship->class = $starship["starship_class"];
                $newStarship->price = is_numeric($starship["cost_in_credits"]) ? $starship["cost_in_credits"] : null;
                $newStarship->save();

                foreach ($starship['pilots'] as $pilot)
                {
                    $pilot_data = $this->get_data_from_url($pilot);
                    if ($pilot_data) {
                        $db_pilot = Person::where('name', $pilot_data['name'])->first();
                        $db_pilot->starships()->attach($newStarship);
                    }
                }

            }

            $url = $data["next"];
            $data = $url ? $this->get_data_from_url($url) : null;
        }
        return true;
    }

    /**
     * Http GET response json from an url
     *  - Includes echo errors
     * 
     * @var string
     */
    private function get_data_from_url($url) {
        $response = Http::get($url);
        if ($response->status() != 200) {
            if ($response["detail"]) {
                echo "\e[1;37;41mError getting data from '".$url."': ".$response->status()." ".$response['detail']."\e[0m\n";
            } else {
                echo "\e[1;37;41mUnknown error\e[0m\n";
            }
            return null;
        }
        return $response->json();
    }
}
